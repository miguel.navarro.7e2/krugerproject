package esp.itb.kruger.lista;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import esp.itb.kruger.R;
import esp.itb.kruger.mapa.MapsActivity;
import esp.itb.kruger.model.Animal;

public class AnimalListFragment extends Fragment {

    private AnimalListViewModel mViewModel;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private AnimalListAdapter adapter;
    private ArrayList<Animal> animales = new ArrayList<Animal>();
    private ArrayList<Animal> animalesPedidos = new ArrayList<>();



    public static AnimalListFragment newInstance() {
        return new AnimalListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.animal_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getActivity());
        adapter = new AnimalListAdapter();
        adapter.setListener(this::onAnimalClicked);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void onAnimalClicked(Animal animal) {
        Intent intent = new Intent(getActivity(), MapsActivity.class);
        intent.putExtra("latitude", animal.getLatitud());
        intent.putExtra("longitude", animal.getLongitud());
        startActivity(intent);
    }

    private void onAnimalChanged(List<Animal> animals) {
        adapter.setList(animals);
        if(animals.isEmpty()){
            Toast.makeText(getContext(), "Empty", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        download();
    }

    @OnClick(R.id.addButton)
    public void onViewClicked(View view){
        download();
        Navigation.findNavController(view).navigate(R.id.actionGoToAddAnimal);
    }

    public void download(){
        FirebaseStorage storage = FirebaseStorage.getInstance("gs://proyectokrueger.appspot.com");
        StorageReference listRef = storage.getReference().child("imagenes/");
        listRef.listAll()
                .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                    @Override
                    public void onSuccess(ListResult listResult) {
                        for (StorageReference prefix : listResult.getPrefixes()) {
                            // All the prefixes under listRef.
                            // You may call listAll() recursively on them.
                        }

                        for (StorageReference item : listResult.getItems()) {
                            downloadFile(item);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Uh-oh, an error occurred!
                    }
                });
    }
    private void downloadFile(StorageReference reference) {
        final long ONE_MEGABYTE = 1024 * 1024;
        reference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                reference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                    @Override
                    public void onSuccess(StorageMetadata storageMetadata) {
                        String latitud = storageMetadata.getCustomMetadata("Latitude");
                        String longitut = storageMetadata.getCustomMetadata("Longitud");
                        String nombre = storageMetadata.getCustomMetadata("Type");
                        String fechaString = storageMetadata.getCustomMetadata("Date");
                        if (fechaString != null) {
                            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
                            Date fecha = null;
                            try {
                                fecha = format.parse(fechaString);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Animal animal = new Animal(bytes, Float.parseFloat(longitut), Float.parseFloat(latitud), nombre,fecha);
                            animales.add(animal);
                            onAnimalChanged(animales);
                        } else {
                            Animal animal = new Animal(bytes, Float.parseFloat(longitut), Float.parseFloat(latitud), nombre);
                            animales.add(animal);
                            onAnimalChanged(animales);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        });
    }

    @OnClick(R.id.reload)
    public void onClickReload(){
        download();
    }

    @OnClick(R.id.allPosts)
    public void onClickAllPosts(){
        onAnimalChanged(animales);
    }

    @OnClick({R.id.dateOrder})
    public void onClickOrderDate(){
        Collections.sort(animales, new Comparator<Animal>() {
            @Override
            public int compare(Animal o1, Animal o2) {
                return o2.fecha.compareTo(o1.fecha);
            }
        });
        onAnimalChanged(animales);
    }

    @OnClick(R.id.tigers)
    public void onClickTigers(){
        changeAnimalList("tiger");
    }

    @OnClick(R.id.lions)
    public void onClickLions(){
        changeAnimalList("lion");
    }

    @OnClick(R.id.elephants)
    public void onClickElephants(){
        changeAnimalList("elephant");
    }

    private void changeAnimalList(String nombreAnimal){
        animalesPedidos.clear();
        for (Animal animal: animales
        ) {
            if (animal.nombreAnimal.equalsIgnoreCase(nombreAnimal)){
                animalesPedidos.add(animal);
            }
        }
        Collections.sort(animalesPedidos, new Comparator<Animal>() {
            @Override
            public int compare(Animal o1, Animal o2) {
                return o2.fecha.compareTo(o1.fecha);
            }
        });
        onAnimalChanged(animalesPedidos);
    }
}
