package esp.itb.kruger.lista;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import esp.itb.kruger.R;
import esp.itb.kruger.model.Animal;

public class AnimalListAdapter extends RecyclerView.Adapter<AnimalListAdapter.MyViewHolder>{

    List<Animal> animalList;
    OnAnimalClickListener listener;
    Context context;

    public AnimalListAdapter(){}

    @NonNull
    @Override
    public AnimalListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.animals_row, parent, false);
        context=parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalListAdapter.MyViewHolder holder, int position) {
        Animal animal = animalList.get(position);
        Bitmap bitmap = BitmapFactory.decodeByteArray(animal.bytes, 0, animal.bytes.length);
        holder.animal.setText(animal.nombreAnimal);
        holder.image.setImageBitmap(bitmap);
        holder.date.setText(animal.fecha.toString());
        holder.itemView.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (animal.nombreAnimal.equalsIgnoreCase("lion")){
                    MediaPlayer media = MediaPlayer.create(context, R.raw.leon);
                    media.start();
                } else if (animal.nombreAnimal.equalsIgnoreCase("elephant")){
                    MediaPlayer media = MediaPlayer.create(context, R.raw.elefante);
                    media.start();

                } else if (animal.nombreAnimal.equalsIgnoreCase("tiger")){
                    MediaPlayer media = MediaPlayer.create(context, R.raw.tigre);
                    media.start();
                }
            }
        });

    }

    public void setListener(OnAnimalClickListener listener){
        this.listener = listener;
    }

    public List<Animal> getAnimalList() {
        return animalList;
    }

    public void setList(List<Animal> list) {
        this.animalList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        int count =0;
        if(animalList!=null){
            count=animalList.size();
        }
        return count;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView animal;
        TextView date;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            image = itemView.findViewById(R.id.animalImage);
            animal = itemView.findViewById(R.id.animalTextView);
            date = itemView.findViewById(R.id.photoDate);
            itemView.setOnClickListener(this::onAnimalClicked);
        }

        public void onAnimalClicked(View view){
            Animal animal = animalList.get(getAdapterPosition());
            listener.onAnimalClicked(animal);
        }
    }

    public interface OnAnimalClickListener {
        void onAnimalClicked (Animal animal);
    }
}
