package esp.itb.kruger.lista;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import esp.itb.kruger.model.Animal;

public class AnimalListViewModel extends AndroidViewModel {
    private LiveData<List<Animal>> animals;

    public AnimalListViewModel(@NonNull Application application, LiveData<List<Animal>> animals) {
        super(application);
        this.animals = animals;
    }

    public LiveData<List<Animal>> getAnimals() {
        return animals;
    }

}
