package esp.itb.kruger.anadir;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import esp.itb.kruger.model.Animal;

public class AddAnimalViewModel extends AndroidViewModel {
    private LiveData<List<Animal>> animals;

    public AddAnimalViewModel(@NonNull Application application) {
        super(application);
    }

}
