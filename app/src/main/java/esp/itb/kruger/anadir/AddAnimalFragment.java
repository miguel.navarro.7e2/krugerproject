package esp.itb.kruger.anadir;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import esp.itb.kruger.R;

import static android.app.Activity.RESULT_OK;

public class AddAnimalFragment extends Fragment {

    private StorageReference referenciaImagenes;
    private String currentPhotoPath;
    private AddAnimalViewModel mViewModel;
    @BindView(R.id.animalAddImage)
    public ImageView animalImage;
    private File fileBitmap;
    private double longitud;
    private double latitud;
    private String timeStamp;

    Uri uriImagen;

    @BindView(R.id.selectType)
    EditText type;


    int PERMISSION_ID = 44;


    private FusedLocationProviderClient fusedLocationProviderClient;

    private Location localizacion;

    public static AddAnimalFragment newInstance() {
        return new AddAnimalFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_animal_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AddAnimalViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
    }

    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                System.out.println(ex.getCause());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        "esp.itb.kruger.fileprovider",
                        photoFile);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            }
        }
    }

    public void subirFoto(Uri uriImagen){
        StorageReference referenciaParaSubir = referenciaImagenes.child(uriImagen.getLastPathSegment());
        String latitudString = String.valueOf(latitud);
        String textType = type.getText().toString();
        String longitudString = String.valueOf(longitud);
        StorageMetadata metadata = new StorageMetadata.Builder().setCustomMetadata("Type", textType).setCustomMetadata("Latitude", latitudString).setCustomMetadata("Longitud", longitudString).setCustomMetadata("Date", timeStamp).build();

        referenciaParaSubir.putFile(uriImagen, metadata)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //Toast.makeText(MainActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        System.out.println(taskSnapshot.getBytesTransferred());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Toast.makeText(MainActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick(R.id.saveButton)
    public void saveImage(){
        subirFoto(uriImagen);
        Navigation.findNavController(getView()).navigate(R.id.action_addAnimalFragment_to_animalListFragment);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
            animalImage.setImageBitmap(imageBitmap);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 0 , bos);
            byte[] bitmapData = bos.toByteArray();
            try {
                FileOutputStream fos = new FileOutputStream(fileBitmap);
                fos.write(bitmapData);
                fos.flush();
                fos.close();
                uriImagen = Uri.parse(fileBitmap.toURI().toString());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseStorage storage = FirebaseStorage.getInstance("gs://proyectokrueger.appspot.com");
        StorageReference referencia = storage.getReference();
        referenciaImagenes = referencia.child("imagenes/");
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "PNG_" + timeStamp + "_";
        fileBitmap = new File(getContext().getCacheDir(), imageFileName);
        try {
            fileBitmap.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (checkPermissions()) {
            getLocation();
            dispatchTakePictureIntent();
        } else {
            requestPermissions();
            getLocation();
            dispatchTakePictureIntent();
        }
    }

    private void getLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        Task location = fusedLocationProviderClient.getLastLocation();
        location.addOnCompleteListener(getActivity(), new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    localizacion = (Location) task.getResult();
                    latitud = localizacion.getLatitude();
                    longitud = localizacion.getLongitude();
                } else {
                    Toast.makeText(getContext(), "Unable to get location", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Granted. Start getting the location information
            }
        }
    }

    @OnClick(R.id.selectType)
    public void type(){
        String[] optionsArray = {"Lion", "Elephant", "Tiger"};
        new MaterialAlertDialogBuilder(getContext())
                .setTitle("Type")
                .setItems(optionsArray, /* listener */ new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0){
                            type.setText(optionsArray[which]);
                        }
                        if (which == 1){
                            type.setText(optionsArray[which]);
                        }
                        if (which == 2){
                            type.setText(optionsArray[which]);
                        }
                    }
                })
                .show();
    }

}
