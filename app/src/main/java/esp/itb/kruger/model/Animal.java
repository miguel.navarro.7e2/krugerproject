package esp.itb.kruger.model;

import java.util.Date;

public class Animal {

    public byte[] bytes;

    public float longitud;

    public float latitud;

    public String nombreAnimal;

    public Date fecha;

    public Animal(byte[] bytes, float longitud, float latitud) {
        this.bytes = bytes;
        this.longitud = longitud;
        this.latitud = latitud;
    }

    public Animal(byte[] bytes, float longitud, float latitud, String nombreAnimal, Date fecha) {
        this.bytes = bytes;
        this.longitud = longitud;
        this.latitud = latitud;
        this.nombreAnimal = nombreAnimal;
        this.fecha = fecha;
    }
    public Animal(byte[] bytes, float longitud, float latitud, String nombreAnimal) {
        this.bytes = bytes;
        this.longitud = longitud;
        this.latitud = latitud;
        this.nombreAnimal = nombreAnimal;
        this.fecha = fecha;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public String getNombreAnimal() {
        return nombreAnimal;
    }

    public void setNombreAnimal(String nombreAnimal) {
        this.nombreAnimal = nombreAnimal;
    }
}
