# David Gilabert i Miguel Navarro

Link al git: https://gitlab.com/miguel.navarro.7e2/krugerproject

## Elements fets

La app es com un Instagram que te una feed on apareixen tots els animals que als que s'hi ha fet una foto i s'ha pujat al nubol
Al fer click a la imatge de un animal et diu la localitzacio de on s'ha fet la foto.

1. Al voler fer una foto a un animal s'obre la camara.

2. Guarda les imatges al nubol amb el firebase storage fent un fitxer que guarda un bitmap i les metadades i es pujen al nubol

3. Cada cop que s'inicia la app es descarreguen totes les fotos que hi ha i es mostren.

4. Al fer click a una imatge obrira el mapa amb la localitzacio de on s'ha fet la foto.

5. Hi ha un boto al costat de la imatge per reproduir el soroll del animal al qual s'ha fet la foto

6. Guardar la localitzacio al firebase.